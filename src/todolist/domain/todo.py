class Todo():
    id: str
    content: str
    completed: bool

    def __init__(self, id, content, completed):
        self.id = id
        self.content = content
        self.completed = completed
    
    def __repr__(self) -> str:
        return self.id + "|" + self.content
    
    def __eq__(self, other): 
        if not isinstance(other, Todo):
            # don't attempt to compare against unrelated types
            return NotImplemented
        return self.id == other.id and self.content == other.content and self.completed == self.completed
    
    def __hash__(self):
        # necessary for instances to behave sanely in dicts and sets.
        return hash((self.id, self.content, self.completed))